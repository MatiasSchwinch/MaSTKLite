![MaSTKLite](https://user-images.githubusercontent.com/93444165/139557803-80b987e7-adba-4dc3-a95e-f38f0ad3aadb.png)

# MaSTK Lite
MaSTK Lite es un gestor de stock fácil de utilizar, programado en C# y WPF.

- Se utiliza Entity Framework 6.0 como ORM y SQLite como sistema de gestión de base de datos.
- Se implementa el patrón de diseño Modelo-Vista-VistaModelo para conectar las propiedades con las vistas, e inyección de dependencias para la clase la cual establece la conexión con la base de datos, y el MainViewModel.
- Se utiliza una librería de controles WPF de terceros llamada HandyControl donde se utilizan algunos controles que esta ofrece.

![MaSTKLite](https://user-images.githubusercontent.com/93444165/139869068-b30c16a5-f721-4d54-a9be-73336dd3af55.gif)

- .NET 5.0
- Windows Presentation Foundation (WPF)
- C#
- Entity Framework 6
- SQLite
